import JohnHeadshot from "../../images/JohnHeadshot.jpg"

const data = {
  play: [
    {
      name: "New York Giants",
      href: "https://www.cbssports.com/nfl/teams/NYG/new-york-giants/",
      src: "https://cdn.shopify.com/s/files/1/1949/1233/products/" + 
        "New_York_Giants_circle.png?v=1575428277",
    },
    {
      name: "New York Knicks",
      href: "https://www.cbssports.com/nba/teams/NY/new-york-knicks/",
      src: "https://upload.wikimedia.org/wikipedia/en/thumb/2/25/" +
        "New_York_Knicks_logo.svg/1200px-New_York_Knicks_logo.svg.png",
    },
    {
      name: "New York Yankees",
      href: "https://www.cbssports.com/mlb/teams/NYY/new-york-yankees/",
      src: "https://upload.wikimedia.org/wikipedia/en/thumb/2/25/" + 
        "NewYorkYankees_PrimaryLogo.svg/1200px-NewYorkYankees_PrimaryLogo.svg.png",
    },
  ],

  programming: [
    {
      name: "CSS for JS Devs",
      href: "https://courses.joshwcomeau.com/css-for-js",
      src: "https://www.joshwcomeau.com/assets/me-light.png",
    },
    {
      name: "Epic React",
      href: "https://epicreact.dev/",
      src: "https://epicreact.dev/static/3e4d61a7372760d641994598a98fca6d/" + 
      "630fb/landing-rocket%402x.png",
    },  
    {
      name: "the Odin Project",
      href: "https://www.theodinproject.com/home",
      src: "https://www.theodinproject.com/assets/" + 
      "og-logo-022832d4cefeec1d5266237be260192f5980f9bcbf1c9ca151b358f0ce1fd2df.png",
    },  
    {
      name: "Reach UI",
      href: "https://reach.tech/",
      src: "https://images.opencollective.com/reach-ui/dd56eb3/logo/256.png",
    },
  ],

  products: [
    {
      name: "Liquid Death",
      href: "https://liquiddeath.com/products/12-pack-tallboys",
      src: "https://upload.wikimedia.org/wikipedia/commons/e/e3/" + 
        "Liquid-Death-Logo.png",
    },
    {
      name: "Barukas",
      href: "https://barukas.com/products/baruka-nuts",
      src: "https://mybariatrickitchenonline.com/wp-content/uploads/2021/02/" + 
        "barukas-logo.png",
    },
    {
      name: "Allbirds",
      href: "https://www.allbirds.com/",
      src: "https://cdn.shopify.com/s/files/1/1104/4168/t/1340/assets/allbirds-logo-fb.jpg?v=9255317745661623995",
    },
  ],

  people: [
    {
      name: "Kent C. Dodds",
      href: "https://kentcdodds.com/",
      src: "https://res.cloudinary.com/kentcdodds-com/image/upload/" + 
        "w_918,q_auto,f_auto/kentcdodds.com/illustrations/kody-flying_red",
    },
    {
      name: "Josh W. Comeau",
      href: "https://www.joshwcomeau.com/",
      src: "https://www.joshwcomeau.com/assets/me-light.png",
    },  
    {
      name: "Emily Morse",
      href: "https://sexwithemily.com/",
      src: "https://bloximages.newyork1.vip.townnews.com/insideradio.com/" + 
        "content/tncms/assets/v3/editorial/f/c1/" + 
        "fc1a1682-27ae-11ec-b79d-0bd447da143c/615f5b9837d95.image.jpg",
    },  
    {
      name: "Darin Olien",
      href: "https://darinolien.com/",
      src: "https://m.media-amazon.com/images/I/41RNu-5zygL._SL500_.jpg",
    },
  ],

  print: [
    {
      name: "Attack on Titan",
      href: "https://ww7.readsnk.com/manga/shingeki-no-kyojin/",
      src: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/" + 
        "b460938b-d443-4b2a-baad-afce23c05d19/" + 
        "d6mq0v4-be5ffe1e-a569-45b3-b3b9-fe9deda15f73.png",
    },
  ],

  peruse: [
    {
      name: "DuckDuckGo",
      href: "https://www.duckduckgo.com",
      src: "https://logos-marques.com/wp-content/uploads/2021/03/DuckDuckGo-Logo-2048x1158.png",
    },
  ],

  portfolio: [
    {
      name: "John Cavaseno",
      href: "https://johncavaseno.com",
      src: JohnHeadshot,
    },
  ],
}

export default data