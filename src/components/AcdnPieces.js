import React from "react"
import styled from "styled-components"
import {  
  Accordion,  
  AccordionItem,  
  AccordionButton,  
  AccordionPanel,
} from "@reach/accordion";
import "@reach/accordion/styles.css";

function AcdnPiece({
  label,
  children,
  column,
}) {
  return (
    <AcdnItm>
      <AcdnLabel>
        <AcdnBtn>{label}</AcdnBtn>
      </AcdnLabel>
      <AcdnPanel>
        { (column === true) ? <ColumnFlex>{children}</ColumnFlex> : children }
      </AcdnPanel>
    </AcdnItm> 
  )
}

const AcdnWrap = styled(Accordion).attrs({
  collapsible: true,
  multiple: true,
})`
  width: min(90%, calc(900 / 16 * 1rem));
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-direction: column;
  gap: calc(var(--NASA) * 3);
`
const AcdnLabel = styled.h2`
  border-radius: var(--NASA);
  background: var(--SKY);
  display: flex;
  justify-content: center;
`
const AcdnItm = styled(AccordionItem)`
  border-radius: var(--NASA);
  border: 1px solid var(--IVORY);
  background: var(--CHARCOAL);
  color: var(--EBONY);
`
const AcdnBtn = styled(AccordionButton)`
  padding: ${8/16}rem;
  color: var(--EBONY);
  width: 100%;
  transition: color 250ms;

  &::before {
    content: ' + ';
  }

  &:focus {
    color: var(--IVORY);
    outline: none;
  }

  &:hover {
    color: var(--IVORY);
  }
`
const AcdnPanel = styled(AccordionPanel)`
  padding: calc(var(--NASA) * 3);
`
const ColumnFlex = styled.div`
  display: flex;
  flex-direction: column;
  gap: calc(var(--NASA) * 3);
`

export { AcdnWrap, AcdnPiece }