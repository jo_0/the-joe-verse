import React, { useRef, useState } from 'react'
import { useSpring, animated, config } from '@react-spring/web'

const calc = (x, y, rect) => [
  -(y - rect.top - rect.height / 2) / 5,
  (x - rect.left - rect.width / 2) / 5,
  1.4
]
const transformer = (x, y, s) =>
  `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`

export default function Card({ children, }) {
  const ref = useRef(null)
  const [xys, set] = useState([0, 0, 1])
  const props = useSpring({ xys, config: config.wobbly, })

  return (
    <div className="card-main" ref={ref}>
      <animated.div
        className="card"
        style={{ 
          '--transform': props.xys.to(transformer),
          transform: 'var(--transform)',
          transition: 'box-shadow 500ms',
        }}
        onMouseLeave={() => set([0, 0, 1])}
        onMouseMove={(e) => {
          const rect = ref.current.getBoundingClientRect()
          set(calc(e.clientX, e.clientY, rect))
        }}
        children={children}
      />
    </div>
  )
}
