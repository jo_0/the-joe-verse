module.exports = {
  siteMetadata: {
    siteUrl: "https://www.josephcorrado.com",
    title: "Hello Joe!",
  },
  plugins: [
    `gatsby-plugin-styled-components`, 
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Hello Joe! | the Portfolio of Joseph Corrado`,
        short_name: `Hello Joe!`,
        start_url: `/`,
        display: `standalone`,
        icon: `src/images/icon.png`,
        background_color: `#616161`,
        theme_color: `#616161`,
      },
    },
  ],
};
